package com.benchmark.mt;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.benchmark.mt.adapter.Level1Adapter;
import com.benchmark.mt.adapter.StringModal;
import com.benchmark.mt.app.Config;
import com.benchmark.mt.app.Dataholder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Level1Activity extends AppCompatActivity {
    private RecyclerView rv;
    private ArrayList<StringModal> mArrayListt;
    private Level1Adapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level1);
        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().show();
        }
        this.getWindow().getDecorView().setBackgroundColor(Color.WHITE);
        rv = findViewById(R.id.recyclerview);
        mArrayListt = new ArrayList<>();
        adapter = new Level1Adapter(this,mArrayListt);

        //RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        //rv.setLayoutManager(mLayoutManager);
        rv.setLayoutManager(new GridLayoutManager(this, 2));
        //rv.setNestedScrollingEnabled(false);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setAdapter(adapter);
        if (getIntent().getExtras()!= null){
            Intent intent = getIntent();
            String key = intent.getStringExtra(Config.K_level1);
            getSupportActionBar().setTitle(key);
            loaddata(key);

        }


    }
    private void loaddata(String key){
        String data = Dataholder.getInstance().getJsondata();
        mArrayListt.clear();
        try {
            JSONObject main = new JSONObject(data);
            JSONObject level1Obj = main.getJSONObject(Config.K_level1);
            JSONArray currentArry = level1Obj.getJSONArray(key);
            for (int i=0;i<currentArry.length();i++){
                StringModal modal = new StringModal();
                modal.setName(currentArry.getString(i));
                mArrayListt.add(modal);
            }
            adapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home)
        {
            onBackPressed();
            return true;
        }
        else
        {
            return super.onOptionsItemSelected(item);
        }

    }
}
