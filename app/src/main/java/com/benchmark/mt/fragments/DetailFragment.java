package com.benchmark.mt.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.benchmark.mt.R;
import com.benchmark.mt.adapter.DetailPageAdapter;
import com.benchmark.mt.adapter.StringModal;
import com.benchmark.mt.app.Config;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailFragment extends Fragment {
    private RecyclerView rv;
    private ArrayList<StringModal> mArrayListt;
    private DetailPageAdapter adapter;

    public DetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        Bundle mBundle = getArguments();
        mArrayListt = (ArrayList<StringModal>) mBundle.getSerializable(Config.K_bundlekey);
        String heading = mBundle.getString(Config.K_detailpageTab);
        TextView tvheading = view.findViewById(R.id.tvheading);
        tvheading.setText(heading);
        rv = view.findViewById(R.id.recyclerview);
        adapter = new DetailPageAdapter(getActivity(),mArrayListt);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(mLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setAdapter(adapter);
        return view;
    }

}
