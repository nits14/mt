package com.benchmark.mt.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.benchmark.mt.R;
import com.benchmark.mt.adapter.DashRvAdapter;
import com.benchmark.mt.adapter.DashRvModal;
import com.benchmark.mt.app.Config;
import com.benchmark.mt.app.Dataholder;
import com.benchmark.mt.app.SimpleDividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {
    private static final String TAG = DashboardFragment.class.getSimpleName();
    private RecyclerView rv;
    private ArrayList<DashRvModal> mArrayListt;
    private DashRvAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        rv = view.findViewById(R.id.rvdashboard);
        swipeRefreshLayout = view.findViewById(R.id.swipeToRefresh);
        mArrayListt = new ArrayList<>();
        adapter = new DashRvAdapter(getActivity(),mArrayListt);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(mLayoutManager);
        //rv.setNestedScrollingEnabled(false);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        rv.setAdapter(adapter);
        createData();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        return view;
    }

    private void createData() {
        mArrayListt.clear();
        try {
            JSONObject mainJson = new JSONObject(Dataholder.getInstance().getJsondata());
            JSONArray mainArray = mainJson.getJSONArray("INDUSTRIAL");
            for (int i=0;i<mainArray.length();i++){
                DashRvModal modal = new DashRvModal();
                modal.setName(mainArray.getString(i));
                modal.setIcon("");
                mArrayListt.add(modal);
            }
            adapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
