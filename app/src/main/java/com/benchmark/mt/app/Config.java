package com.benchmark.mt.app;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Admin on 021 21-01-2019.
 */

public class Config {

    public static String K_level1 = "level1";
    public static String K_level2 = "level2";
    public static String K_level3 = "level3";
    public static String K_level4 = "level4";
    public static String K_detailpage = "detailpage";
    public static String K_detailpageTab = "detailpagetab";
    public static String K_bundlekey = "bundlekey";
    public static String K_Snack_foods = "SNACK FOODS";
    public static String K_MainActivityTitle = "Industrial Segments";

    public static String K_Responsibilities = "Responsibilities";
    public static String K_Concerns = "Main Concerns";
    public static String K_Sales_approach = "Sales Approach";


    //Sequence wise added
    public enum Menus{
        Dashboard,
        Profile,
        Food
    }


    public static String loadJSONFromAsset(Context ctx) {
        String json;
        try {
            InputStream is =  ctx.getAssets().open("data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
