package com.benchmark.mt.app;

/**
 * Created by Admin on 023 23-01-2019.
 */

public class Dataholder {

    private String jsondata;
    private static Dataholder instance;

    public Dataholder() {}

    public static Dataholder getInstance() {
        if( instance == null ) {
            instance = new Dataholder();
        }
        return instance;
    }

    public String getJsondata() {
        return jsondata;
    }

    public void setJsondata(String jsondata) {
        this.jsondata = jsondata;
    }
}
