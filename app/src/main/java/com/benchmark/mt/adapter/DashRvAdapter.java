package com.benchmark.mt.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.benchmark.mt.Level1Activity;
import com.benchmark.mt.R;
import com.benchmark.mt.app.Config;
import com.benchmark.mt.app.Dataholder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Admin on 021 21-01-2019.
 */

public class DashRvAdapter extends RecyclerView.Adapter<DashRvAdapter.Myviewholder> {
    private Context mContext;
    private ArrayList<DashRvModal> mArrayListt; //TODO change

    public DashRvAdapter(Context mContext, ArrayList<DashRvModal> mArrayListt) {
        this.mContext = mContext;
        this.mArrayListt = mArrayListt;
    }

    @NonNull
    @Override
    public Myviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(mContext).inflate(R.layout.carditem_dash,parent,false);
        return new Myviewholder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull Myviewholder holder, int position) {
        DashRvModal modal = mArrayListt.get(holder.getAdapterPosition());
        final String nm = modal.getName();
        //holder.ivcardicon.setImageBitmap();
        holder.tvcardname.setText(nm);
        if (nm.contains("PHARMA")){
            holder.ivcardicon.setBackgroundResource(R.drawable.pharma);
        }else if (nm.contains("CHEMICALS")){
            holder.ivcardicon.setBackgroundResource(R.drawable.chemical);
        }else if (nm.contains("FOOD")){
            holder.ivcardicon.setBackgroundResource(R.drawable.food);
        }else if (nm.contains("TESTING")){
            holder.ivcardicon.setBackgroundResource(R.drawable.testing);
        }else if (nm.contains("ACADEMIA")){
            holder.ivcardicon.setBackgroundResource(R.drawable.academia);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String data = Dataholder.getInstance().getJsondata();
                try {
                    JSONObject main = new JSONObject(data);
                    JSONObject level1 = main.getJSONObject(Config.K_level1);
                    if (level1.has(nm)){
                        Log.d("level1","Yes object found:"+nm);
                        Intent intent = new Intent(mContext, Level1Activity.class);
                        intent.putExtra(Config.K_level1, nm);
                        mContext.startActivity(intent);

                    }else {
                        Log.d("level1","No object found:"+nm);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
              //  MainActivity.navItemIndex = Config.Menus.Food.ordinal();
              //  MainActivity.CURRENT_TAG = Config.Menus.Food.name();

              //  if (mContext!= null) {
             //       ((MainActivity) mContext).loadHomeFragment();
              //  }

            }
        });


    }

    @Override
    public int getItemCount() {
        return mArrayListt.size();
    }


    class Myviewholder extends RecyclerView.ViewHolder{
        TextView tvcardname;
        ImageView ivcardicon;

        public Myviewholder(View itemView) {
            super(itemView);
            tvcardname = itemView.findViewById(R.id.tvcardname);
            ivcardicon = itemView.findViewById(R.id.ivcardicon);
        }
    }
}
