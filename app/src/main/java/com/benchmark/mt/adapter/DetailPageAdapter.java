package com.benchmark.mt.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.benchmark.mt.R;

import java.util.ArrayList;

/**
 * Created by Admin on 025 25-01-2019.
 */

public class DetailPageAdapter extends RecyclerView.Adapter<DetailPageAdapter.MyviewHolder>{
    private Context mContext;
    private ArrayList<StringModal> mArrayListt; //TODO change

    public DetailPageAdapter(Context mContext, ArrayList<StringModal> mArrayListt) {
        this.mContext = mContext;
        this.mArrayListt = mArrayListt;
    }

    @NonNull
    @Override
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(mContext).inflate(R.layout.card_item_detail,parent,false);
        return new MyviewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, int position) {
        StringModal modal = mArrayListt.get(holder.getAdapterPosition());
        holder.tvcardtextdetail.setText(modal.getName());
    }

    @Override
    public int getItemCount() {
        return mArrayListt.size();
    }

    class MyviewHolder extends RecyclerView.ViewHolder{
        TextView tvcardtextdetail;

        public MyviewHolder(View itemView) {
            super(itemView);
            tvcardtextdetail = itemView.findViewById(R.id.tvcardtextdetail);
        }
    }
}
