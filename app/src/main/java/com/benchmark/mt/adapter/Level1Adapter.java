package com.benchmark.mt.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.benchmark.mt.Level1Activity;
import com.benchmark.mt.Level2Activity;
import com.benchmark.mt.R;
import com.benchmark.mt.app.Config;
import com.benchmark.mt.app.Dataholder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Admin on 023 23-01-2019.
 */

public class Level1Adapter extends RecyclerView.Adapter<Level1Adapter.Myviewholder>{
    private Context mContext;
    private ArrayList<StringModal> mArrayListt; //TODO change

    public Level1Adapter(Context mContext, ArrayList<StringModal> mArrayListt) {
        this.mContext = mContext;
        this.mArrayListt = mArrayListt;
    }

    @NonNull
    @Override
    public Myviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(mContext).inflate(R.layout.card_level1,parent,false);
        return new Myviewholder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull Myviewholder holder, int position) {
        final StringModal modal = mArrayListt.get(holder.getAdapterPosition());
/*        if (position%2==0)
        holder.llcard.setGravity(Gravity.START);
        else
            holder.llcard.setGravity(Gravity.END);*/
        String name = modal.getName();
        String upperString = name.substring(0,1).toUpperCase() + name.substring(1).toLowerCase();
        holder.textView.setText(upperString);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nm = modal.getName();
                String data = Dataholder.getInstance().getJsondata();
                try {
                    JSONObject main = new JSONObject(data);
                    JSONObject level2 = main.getJSONObject(Config.K_level2);
                    if (level2.has(nm)){
                        Log.d("level2","Yes object found:"+nm);
                        Intent intent = new Intent(mContext, Level2Activity.class);
                        intent.putExtra(Config.K_level2, nm);
                        mContext.startActivity(intent);

                    }else {
                        Log.d("level2","No object found:"+nm);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayListt.size();
    }

    public class Myviewholder extends RecyclerView.ViewHolder{
        TextView textView;
        LinearLayout llcard;

        private Myviewholder(View itemView) {
            super(itemView);
            llcard = itemView.findViewById(R.id.llcard);
            textView = itemView.findViewById(R.id.tvlbl);
        }
    }
}
