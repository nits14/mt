package com.benchmark.mt.adapter;

/**
 * Created by Admin on 021 21-01-2019.
 */

public class DashRvModal {
    String name;
    String icon;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
