package com.benchmark.mt;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.Toast;

import com.benchmark.mt.adapter.DetailPageAdapter;
import com.benchmark.mt.adapter.Level2Adapter;
import com.benchmark.mt.adapter.Level3Adapter;
import com.benchmark.mt.adapter.StringModal;
import com.benchmark.mt.app.Config;
import com.benchmark.mt.app.Dataholder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Level3Activity extends AppCompatActivity {
    private static final String TAG = Level3Activity.class.getSimpleName();
    private RecyclerView rv;
    private ArrayList<StringModal> mArrayListt;
    private Level3Adapter adapter;
    private DetailPageAdapter adapter2;
    private String key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level3);

        this.getWindow().getDecorView().setBackgroundColor(Color.WHITE);
        if (getIntent().getExtras()!= null){
            Intent intent = getIntent();
            key = intent.getStringExtra(Config.K_level3);
            if (getSupportActionBar()!=null){
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().show();
                getSupportActionBar().setTitle(key);
            }
            mArrayListt = new ArrayList<>();
            loaddata(key);



            rv = findViewById(R.id.recyclerview);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            rv.setLayoutManager(mLayoutManager);
            //rv.setNestedScrollingEnabled(false);
            rv.setItemAnimator(new DefaultItemAnimator());
            if (key.contains("IMPORTANT WORK PLACES")){
                adapter2 = new DetailPageAdapter(this,mArrayListt);
                rv.setAdapter(adapter2);
                adapter2.notifyDataSetChanged();
            }else {
                adapter = new Level3Adapter(this,mArrayListt);
                rv.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        }else {
            Toast.makeText(this,"Data not found",Toast.LENGTH_SHORT).show();
        }

    }

    private void loaddata(String key){
        String data = Dataholder.getInstance().getJsondata();
        mArrayListt.clear();
        try {
            JSONObject main = new JSONObject(data);
            JSONObject level3Obj = main.getJSONObject(Config.K_level3);
            JSONArray level3Array = level3Obj.getJSONArray(key);
            for (int i=0;i<level3Array.length();i++){
                StringModal modal = new StringModal();
                modal.setName(level3Array.getString(i));
                mArrayListt.add(modal);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home)
        {
            onBackPressed();
            return true;
        }
        else
        {
            return super.onOptionsItemSelected(item);
        }

    }
}
