package com.benchmark.mt;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.benchmark.mt.adapter.StringModal;
import com.benchmark.mt.app.Config;
import com.benchmark.mt.app.Dataholder;
import com.benchmark.mt.fragments.DetailFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DetailActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private String detailof;
    private ArrayList<StringModal> list1,list2,list3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.getWindow().getDecorView().setBackgroundColor(Color.WHITE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = findViewById(R.id.viewpager);

        list1 = new ArrayList<>();
        list2 = new ArrayList<>();
        list3 = new ArrayList<>();

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        if (getIntent().getExtras()!= null){
            Intent intent = getIntent();
            detailof = intent.getStringExtra(Config.K_detailpage);
            getSupportActionBar().setTitle(detailof);

            setupViewPager(viewPager);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new DetailFragment(), Config.K_Responsibilities);
        adapter.addFragment(new DetailFragment(),Config.K_Concerns);
        adapter.addFragment(new DetailFragment(), Config.K_Sales_approach);
        viewPager.setAdapter(adapter);
        String data = Dataholder.getInstance().getJsondata();
        try {
            JSONObject dataobj = new JSONObject(data);
            JSONObject detailobj = dataobj.getJSONObject(Config.K_detailpage);
            JSONObject activityDetailObj = detailobj.getJSONObject(detailof);
            JSONArray respoArray = activityDetailObj.getJSONArray(Config.K_Responsibilities);
            JSONArray concernArray = activityDetailObj.getJSONArray(Config.K_Concerns);
            JSONArray salesArray = activityDetailObj.getJSONArray(Config.K_Sales_approach);
            list1.clear();
            list2.clear();
            list3.clear();
            for (int i = 0; i < respoArray.length(); i++) {
                StringModal modal = new StringModal();
                modal.setName(respoArray.getString(i));
                list1.add(modal);
            }
            for (int i = 0; i < concernArray.length(); i++) {
                StringModal modal = new StringModal();
                modal.setName(concernArray.getString(i));
                list2.add(modal);
            }
            for (int i = 0; i < salesArray.length(); i++) {
                StringModal modal = new StringModal();
                modal.setName(salesArray.getString(i));
                list3.add(modal);
            }
            adapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
           // return mFragmentList.get(position);
            switch (position){
                case 0:
                    Bundle bundlelist1 = new Bundle();
                    bundlelist1.putSerializable(Config.K_bundlekey,list1);
                    bundlelist1.putString(Config.K_detailpageTab,Config.K_Responsibilities);
                    Fragment fragment1 = mFragmentList.get(position);
                    fragment1.setArguments(bundlelist1);
                    return fragment1;

                case 1:
                    Bundle bundlelist2 = new Bundle();
                    bundlelist2.putSerializable(Config.K_bundlekey,list2);
                    bundlelist2.putString(Config.K_detailpageTab,Config.K_Concerns);
                    Fragment fragment2 = mFragmentList.get(position);
                    fragment2.setArguments(bundlelist2);
                    return fragment2;

                case 2:
                    Bundle bundlelist3 = new Bundle();
                    bundlelist3.putSerializable(Config.K_bundlekey,list3);
                    bundlelist3.putString(Config.K_detailpageTab,Config.K_Sales_approach);
                    Fragment fragment3 = mFragmentList.get(position);
                    fragment3.setArguments(bundlelist3);
                    return fragment3;

                    default: return null;
            }

        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }
        @Override
        public int getItemPosition(@NonNull Object object) {
            // return super.getItemPosition(object);
            return POSITION_NONE;
        }
        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
